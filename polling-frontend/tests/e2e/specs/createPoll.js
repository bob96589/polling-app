module.exports = {
  'create poll test': browser => {
    browser
      .url(process.env.VUE_DEV_SERVER_URL + '#/login')
      .waitForElementVisible('body', 2000)
      .useXpath()
      .setValue('//*[@id="username"]', 'bob123')
      .setValue('//*[@id="password"]', '123456')
      .click('//*[@id="btnLogin"]')
      .pause(2000)
      .assert.urlContains('/home')
      .click('//*[@id="createPoll"]')
      .pause(2000)
      .assert.urlContains('/createPoll')
      .setValue('//*[@id="container"]/div/div[2]/div[1]/textarea', 'a,b,c?')
      .setValue('//*[@id="container"]/div/div[2]/div[2]/input', 'a')
      .setValue('//*[@id="container"]/div/div[2]/div[3]/input', 'b')
      .click('//*[@id="container"]/div/div[2]/button[1]')
      .setValue('//*[@id="container"]/div/div[2]/div[4]/div/input', 'c')
      .click('//*[@id="container"]/div/div[2]/button[2]')
      .pause(2000)
      .assert.urlContains('/home')
      .end()
  }
}
