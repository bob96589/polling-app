// For authoring Nightwatch tests, see
// http://nightwatchjs.org/guide#usage

module.exports = {
  'login logout test': browser => {
    browser
      .url(process.env.VUE_DEV_SERVER_URL + '#/login')
      .waitForElementVisible('body', 1000)
      .setValue('#username', 'bob123')
      .setValue('#password', '123456')
      .click('#btnLogin')
      .pause(2000)
      .assert.urlContains('/home')
      .click('#userPopup')
      .click('#logout')
      .pause(1000)
      .assert.urlContains('/login')
      .end()
  }
}
