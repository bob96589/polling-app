import filter from '@/filters/filter'

describe('filter.js', () => {
  it('should return first char of the input string', () => {
    var result = filter.firstChar('Apple')
    expect(result).toMatch('A')
  })

  it('should return first char of the input string', () => {
    var result = filter.formatDateTime('2019-03-03T16:24:09.000+0000')
    expect(result).toMatch('2019-03-04 00:24:09')
    expect(result).toMatch(/^\d{4}-[01]\d-[0-3]\d [0-2]\d:[0-5]\d:[0-5]\d$/)
  })
})
