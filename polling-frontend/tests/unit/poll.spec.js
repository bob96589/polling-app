import {
  createLocalVue,
  shallowMount
} from '@vue/test-utils'
import Vuex from 'vuex'
import Poll from '@/components/Poll.vue'
import Common from '@/utils/common'
import '@/filters'

const localVue = createLocalVue()
localVue.use(Common)
localVue.use(Vuex)

describe('Poll.vue - Admin', () => {
  let auth
  let store

  beforeEach(() => {
    auth = {
      namespaced: true,
      getters: {
        isAuthenticated () {
          return true
        },
        userInfo () {
          return {
            token: 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiI3M2FlYTQ3Zi00MWQ5LTQzMjgtYjgzYi1lOTk5MTRhNjFhZWIiLCJpc3MiOiJodHRwOi8vYm9iLm9yZyIsImF1ZCI6Imh0dHA6Ly9ib2Iub3JnIiwic3ViIjoiYm9iMTIzIiwiaWF0IjoxNTU0NDY4MzEzLCJleHAiOjE1NTQ0NzAxMTMsInJlZnJlc2hDb3VudCI6MCwicmVmcmVzaExpbWl0IjozfQ.59cYJSVqIJhSy6nizXcB6vjXYEFEoH3gR6ZtU1nWaI8',
            username: 'bob123',
            name: 'Bob Peng',
            authorities: 'Admin'
          }
        }
      }
    }
    store = new Vuex.Store({
      modules: {
        auth
      }
    })
  })

  const poll = {
    'id': 'ab2adfe27d704075a029818d1b16083e',
    'question': 'a,b,c?',
    'choices': [{
      'id': '001a2c73fd834ff08aa621b418b2586f',
      'text': 'b',
      'voteCount': 0
    }, {
      'id': '3a54f830b5ff4077baef3a665b4b8f38',
      'text': 'c',
      'voteCount': 0
    }],
    'creator': {
      'id': 'aeb565cf69cd41a1b1c9471c2896d9b0',
      'username': 'bob123',
      'name': 'Bob Peng',
      'authorities': null
    },
    'createTime': '2019-04-01 22:27:13',
    'expireTime': '2019-04-20 22:27:13',
    'totalVotes': 0,
    'expired': false
  }

  it('choice count', () => {
    const wrapper = shallowMount(Poll, {
      localVue,
      store,
      propsData: {
        poll
      }
    })
    expect(wrapper.findAll('.choice').length).toBe(2)
  })

  it('button count', () => {
    const wrapper = shallowMount(Poll, {
      localVue,
      store,
      propsData: {
        poll
      }
    })
    expect(wrapper.findAll('button.btn-circle').length).toBe(0)
    expect(wrapper.findAll('button:not(.btn-circle)').length).toBe(2)
  })

  it('delete button', () => {
    const wrapper = shallowMount(Poll, {
      localVue,
      store,
      propsData: {
        poll
      }
    })
    const deletePollMock = jest.fn()
    wrapper.vm.deletePoll = deletePollMock

    wrapper.findAll('button').at(1).trigger('click')
    expect(deletePollMock).toHaveBeenCalled()
  })

  it('vote button', () => {
    const wrapper = shallowMount(Poll, {
      localVue,
      store,
      propsData: {
        poll
      }
    })

    // vote
    const voteMock = jest.fn()
    wrapper.vm.vote = voteMock

    let voteBtn = wrapper.findAll('button').at(0)
    let firstChoice = wrapper.findAll('input[type="radio"]').at(0)

    expect(voteBtn.attributes('disabled')).toBe('disabled')
    firstChoice.trigger('click')
    expect(voteBtn.attributes('disabled')).toBe(undefined)
    voteBtn.trigger('click')
    expect(voteMock).toHaveBeenCalled()
  })
})

describe('Poll.vue - User', () => {
  let auth
  let store

  beforeEach(() => {
    auth = {
      namespaced: true,
      getters: {
        isAuthenticated () {
          return true
        },
        userInfo () {
          return {
            token: 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiI3M2FlYTQ3Zi00MWQ5LTQzMjgtYjgzYi1lOTk5MTRhNjFhZWIiLCJpc3MiOiJodHRwOi8vYm9iLm9yZyIsImF1ZCI6Imh0dHA6Ly9ib2Iub3JnIiwic3ViIjoiYm9iMTIzIiwiaWF0IjoxNTU0NDY4MzEzLCJleHAiOjE1NTQ0NzAxMTMsInJlZnJlc2hDb3VudCI6MCwicmVmcmVzaExpbWl0IjozfQ.59cYJSVqIJhSy6nizXcB6vjXYEFEoH3gR6ZtU1nWaI8',
            username: 'bob123',
            name: 'Bob Peng',
            authorities: 'User'
          }
        }
      }
    }
    store = new Vuex.Store({
      modules: {
        auth
      }
    })
  })

  const poll = {
    'id': 'ab2adfe27d704075a029818d1b16083e',
    'question': 'a,b,c?',
    'choices': [{
      'id': '001a2c73fd834ff08aa621b418b2586f',
      'text': 'b',
      'voteCount': 0
    }, {
      'id': '3a54f830b5ff4077baef3a665b4b8f38',
      'text': 'c',
      'voteCount': 0
    }, {
      'id': '3a54f830b5ff4077baef3a665b4b8f44',
      'text': 'd',
      'voteCount': 0
    }],
    'creator': {
      'id': 'aeb565cf69cd41a1b1c9471c2896d9b0',
      'username': 'bob123',
      'name': 'Bob Peng',
      'authorities': null
    },
    'createTime': '2019-04-01 22:27:13',
    'expireTime': '2019-04-20 22:27:13',
    'totalVotes': 0,
    'expired': false
  }

  it('choice count', () => {
    const wrapper = shallowMount(Poll, {
      localVue,
      store,
      propsData: {
        poll
      }
    })
    expect(wrapper.findAll('.choice').length).toBe(3)
  })

  it('button count', () => {
    const wrapper = shallowMount(Poll, {
      localVue,
      store,
      propsData: {
        poll
      }
    })
    expect(wrapper.findAll('button.btn-circle').length).toBe(0)
    expect(wrapper.findAll('button:not(.btn-circle)').length).toBe(1)
  })
})

describe('Poll.vue - Unauthenticated user ', () => {
  let auth
  let store

  beforeEach(() => {
    auth = {
      namespaced: true,
      getters: {
        isAuthenticated () {
          return false
        },
        userInfo () {
          return {
            token: null,
            username: null,
            name: null,
            authorities: null
          }
        }
      }
    }
    store = new Vuex.Store({
      modules: {
        auth
      }
    })
  })

  const poll = {
    'id': 'ab2adfe27d704075a029818d1b16083e',
    'question': 'a,b,c?',
    'choices': [{
      'id': '001a2c73fd834ff08aa621b418b2586f',
      'text': 'b',
      'voteCount': 0
    }, {
      'id': '3a54f830b5ff4077baef3a665b4b8f38',
      'text': 'c',
      'voteCount': 0
    }, {
      'id': '3a54f830b5ff4077baef3a665b4b8f44',
      'text': 'd',
      'voteCount': 0
    }],
    'creator': {
      'id': 'aeb565cf69cd41a1b1c9471c2896d9b0',
      'username': 'bob123',
      'name': 'Bob Peng',
      'authorities': null
    },
    'createTime': '2019-04-01 22:27:13',
    'expireTime': '2019-04-20 22:27:13',
    'totalVotes': 0,
    'expired': false
  }

  it('choice count', () => {
    const wrapper = shallowMount(Poll, {
      localVue,
      store,
      propsData: {
        poll
      }
    })
    expect(wrapper.findAll('.result').length).toBe(3)
  })

  it('button count', () => {
    const wrapper = shallowMount(Poll, {
      localVue,
      store,
      propsData: {
        poll
      }
    })
    expect(wrapper.findAll('button.btn-circle').length).toBe(3)
    expect(wrapper.findAll('button:not(.btn-circle)').length).toBe(0)
  })
})

describe('Poll.vue - Expired Poll', () => {
  let auth
  let store

  beforeEach(() => {
    auth = {
      namespaced: true,
      getters: {
        isAuthenticated () {
          return true
        },
        userInfo () {
          return {
            token: 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiI3M2FlYTQ3Zi00MWQ5LTQzMjgtYjgzYi1lOTk5MTRhNjFhZWIiLCJpc3MiOiJodHRwOi8vYm9iLm9yZyIsImF1ZCI6Imh0dHA6Ly9ib2Iub3JnIiwic3ViIjoiYm9iMTIzIiwiaWF0IjoxNTU0NDY4MzEzLCJleHAiOjE1NTQ0NzAxMTMsInJlZnJlc2hDb3VudCI6MCwicmVmcmVzaExpbWl0IjozfQ.59cYJSVqIJhSy6nizXcB6vjXYEFEoH3gR6ZtU1nWaI8',
            username: 'bob123',
            name: 'Bob Peng',
            authorities: 'User'
          }
        }
      }
    }
    store = new Vuex.Store({
      modules: {
        auth
      }
    })
  })

  const poll = {
    'id': 'ab2adfe27d704075a029818d1b16083e',
    'question': 'a,b,c?',
    'choices': [{
      'id': '001a2c73fd834ff08aa621b418b2586f',
      'text': 'b',
      'voteCount': 0
    }, {
      'id': '3a54f830b5ff4077baef3a665b4b8f38',
      'text': 'c',
      'voteCount': 0
    }, {
      'id': '3a54f830b5ff4077baef3a665b4b8f44',
      'text': 'd',
      'voteCount': 0
    }],
    'creator': {
      'id': 'aeb565cf69cd41a1b1c9471c2896d9b0',
      'username': 'bob123',
      'name': 'Bob Peng',
      'authorities': null
    },
    'createTime': '2019-04-01 22:27:13',
    'expireTime': '2019-04-20 22:27:13',
    'totalVotes': 0,
    'expired': true
  }

  it('choice count', () => {
    const wrapper = shallowMount(Poll, {
      localVue,
      store,
      propsData: {
        poll
      }
    })
    expect(wrapper.findAll('.result').length).toBe(3)
  })

  it('button count', () => {
    const wrapper = shallowMount(Poll, {
      localVue,
      store,
      propsData: {
        poll
      }
    })
    expect(wrapper.findAll('button.btn-circle').length).toBe(3)
    expect(wrapper.findAll('button:not(.btn-circle)').length).toBe(0)
  })
})
