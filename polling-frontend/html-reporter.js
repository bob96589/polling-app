var HtmlReporter = require('nightwatch-html-reporter')
var reporter = new HtmlReporter({
  openBrowser: true,
  reportsDirectory: 'tests/e2e/reports',
  reportFilename: 'e2eReport.html'
})

module.exports = {
  write: function (results, options, done) {
    reporter.fn(results, done)
  }
}
