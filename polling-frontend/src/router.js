import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const router = new Router({
  routes: [ {
    path: '/',
    redirect: '/home'
  }, {
    path: '/',
    name: 'home',
    component: () => import('./views/Home.vue')
  }, {
    path: '/home',
    name: 'home',
    component: () => import('./views/Home.vue')
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('./views/Login.vue')
  },
  {
    path: '/signup',
    name: 'signup',
    component: () => import('./views/Signup.vue')
  },
  {
    path: '/createPoll',
    name: 'createPoll',
    meta: {
      requiresAuth: true
    },
    component: () => import('./views/CreatePoll.vue')
  },
  {
    path: '/profile/:username',
    name: 'profile',
    meta: {
      requiresAuth: true
    },
    component: () => import('./views/Profile.vue')
  }
  ]
})

router.beforeEach((to, from, next) => {
  let requiresAuth = to.matched.some(record => {
    // console.log(to.fullPath, '>> ', from.fullPath, ', requiresAuth: ', record.meta.requiresAuth)
    return record.meta.requiresAuth
  })
  if (requiresAuth && !localStorage.getItem('token')) {
    next({
      path: '/login'
    })
  } else {
    next()
  }
})

export default router
