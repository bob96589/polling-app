import MsgModal from '../components/MsgModal.vue'
import ConfirmModal from '../components/ConfirmModal.vue'
import Vue from 'vue'
export default {
  install () {
    Vue.prototype.$common = {}
    Vue.common = {
      showMessage (vueComponent, msg) {
        var ComponentClass = Vue.extend(MsgModal)
        var instance = new ComponentClass()
        instance.$slots.default = [msg]
        instance.$mount()
        vueComponent.$el.appendChild(instance.$el)
        instance.show()
      },
      showConfirmModal (vueComponent, { msg, action }) {
        var ComponentClass = Vue.extend(ConfirmModal)
        var instance = new ComponentClass()
        instance.sureAction = action
        instance.$slots.default = [msg]
        instance.$mount()
        vueComponent.$el.appendChild(instance.$el)
        instance.show()
      },
      stringToColor (str) {
        str += 'aaa'
        let hash = 0
        for (let i = 0; i < str.length; i++) {
          hash = str.charCodeAt(i) + ((hash << 5) - hash)
        }
        let color = '#'
        for (let i = 0; i < 3; i++) {
          let value = (hash >> (i * 8)) & 0xFF
          color += ('00' + value.toString(16)).substr(-2)
        }
        return color
      },
      isTokenExpired (token) {
        var period = parseJwt(token).exp * 1000 - new Date().getTime()
        return period < 0
      },
      shouldRefreshToken (token, url) {
        var period = parseJwt(token).exp * 1000 - new Date().getTime()
        return period > 0 && period < process.env.VUE_APP_REFRESH_BEFORE_EXPIRATION && url !== '/auth/refresh'
      }
    }
  }
}

function parseJwt (token) {
  var base64Url = token.split('.')[1]
  var base64 = base64Url.replace('-', '+').replace('_', '/')
  return JSON.parse(window.atob(base64))
}
