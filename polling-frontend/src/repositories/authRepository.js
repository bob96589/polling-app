import Vue from 'vue'

const resource = '/auth'
export default {
  signin (payload) {
    return Vue.axios.post(`${resource}/signin`, payload)
  },
  signup (payload) {
    return Vue.axios.post(`${resource}/signup`, payload)
  },
  refreshToken () {
    return Vue.axios.post(`${resource}/refresh`)
  }
}
