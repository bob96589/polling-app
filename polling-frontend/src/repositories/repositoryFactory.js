import PollRepository from './pollRepository'
import UserRepository from './userRepository'
import AuthRepository from './authRepository'

const repositories = {
  PollRepository,
  UserRepository,
  AuthRepository
}

export const RepositoryFactory = {
  get: name => repositories[name]
}
