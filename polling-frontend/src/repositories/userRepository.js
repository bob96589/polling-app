import Vue from 'vue'

const resource = '/users'
export default {
  getCurrentUser () {
    return Vue.axios.get(`user/me`)
  },
  getUserProfile (username) {
    return Vue.axios.get(`${resource}/${username}`)
  },
  getPollsCreatedBy (username, page, size) {
    return Vue.axios.get(`${resource}/${username}/polls` + (page && size ? `?page=${page}&size=${size}` : ''))
  },
  getPollsVotedBy (username, page, size) {
    return Vue.axios.get(`${resource}/${username}/votes` + (page && size ? `?page=${page}&size=${size}` : ''))
  },
  getUsersByChoiceId (choiceId) {
    return Vue.axios.get(`${resource}?choiceId=${choiceId}`)
  }
}
