import Vue from 'vue'

const resource = '/polls'
export default {
  getPolls (listType, page, size) {
    return Vue.axios.get(`${resource}` + `?listType=${listType}` + (page && size ? `&page=${page}&size=${size}` : ''))
  },
  createPoll (payload) {
    return Vue.axios.post(`${resource}`, payload)
  },
  getPollById (pollId) {
    return Vue.axios.get(`${resource}/${pollId}`)
  },
  castVote (pollId, payload) {
    return Vue.axios.post(`${resource}/${pollId}/votes`, payload)
  },
  deletePoll (pollId) {
    return Vue.axios.delete(`${resource}/${pollId}`)
  }
}
