import Vue from 'vue'
import VueAxios from 'vue-axios'
import axios from 'axios'
import store from '@/store'
import router from '@/router'

Vue.use(VueAxios, axios)

const baseDomain = process.env.VUE_APP_BACKEND_DOMAIN
Vue.axios.defaults.baseURL = `${baseDomain}/api`

axios.interceptors.request.use(
  (config) => {
    let token = localStorage.getItem('token')
    if (token) {
      if (Vue.common.shouldRefreshToken(token, config.url)) {
        axios.post('/auth/refresh').then(res => {
          let data = res.data
          let newToken = data.tokenType + ' ' + data.accessToken
          localStorage.setItem('token', newToken)
          store.commit('auth/updateToken', { token: newToken })
        }).catch(error => {
          console.log(error)
        })
      }
      config.headers['Authorization'] = token
    }
    store.dispatch('loading/triggerLoading', true)
    return config
  },
  (error) => {
    store.dispatch('loading/triggerLoading', false)
    return Promise.reject(error)
  }
)

axios.interceptors.response.use((response) => {
  store.dispatch('loading/triggerLoading', false)
  return response
}, (error) => {
  store.dispatch('loading/triggerLoading', false)
  if (error.response.status === 401) {
    setTimeout(function () {
      store.dispatch('auth/logout').then(() => {
        router.push('login')
      })
    }, 2000)
  }
  return Promise.reject(error)
})
