import { RepositoryFactory } from '@/repositories/repositoryFactory'
const UserRepository = RepositoryFactory.get('UserRepository')
const AuthRepository = RepositoryFactory.get('AuthRepository')

const state = {
  token: null,
  username: null,
  name: null,
  authorities: null
}

const mutations = {
  authUser (state, userData) {
    state.username = userData.username
    state.name = userData.name
    state.authorities = userData.authorities
    state.token = userData.token
  },
  logout (state) {
    state.username = null
    state.name = null
    state.authorities = null
    state.token = null
  },
  updateToken (state, { token }) {
    state.token = token
  }
}

const actions = {
  login ({ dispatch }, authData) {
    return new Promise((resolve, reject) => {
      AuthRepository.signin(authData).then(res => {
        let data = res.data
        let token = data.tokenType + ' ' + data.accessToken
        localStorage.setItem('token', token)
        dispatch('loadMe')
        resolve(res)
      }).catch(error => {
        console.log(error)
        reject(error)
      })
    })
  },
  loadMe ({ commit }) {
    UserRepository.getCurrentUser().then(res => {
      let data = res.data
      commit('authUser', {
        username: data.username,
        name: data.name,
        authorities: data.authorities,
        token: localStorage.getItem('token')
      })
    })
  },
  logout ({ commit }) {
    return new Promise((resolve, reject) => {
      localStorage.removeItem('token')
      commit('logout')
      resolve()
    })
  }
}

const getters = {
  isAuthenticated (state, getters) {
    return state.token !== null
  },
  userInfo (state, getters) {
    return state
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
