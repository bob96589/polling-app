const state = {
  isLoading: false,
  refCount: 0
}

const mutations = {
  triggerLoading (state, flag) {
    state.isLoading = flag
  }
}

const actions = {
  triggerLoading ({ state, commit }, flag) {
    if (flag) {
      state.refCount++
      commit('triggerLoading', true)
    } else if (state.refCount > 0) {
      state.refCount--
      commit('triggerLoading', this.refCount > 0)
    }
  }
}

const getters = {
  isLoading (state) {
    return state.isLoading
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
