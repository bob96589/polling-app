export default {
  firstChar (item) {
    return item ? item.substr(0, 1) : item
  },
  dateTimeCompareToNow (item) {
    let nowTime = new Date().getTime()
    let minuteTime = 60 * 1000
    let hourTime = 60 * minuteTime
    let dayTime = 24 * hourTime
    let monthTime = dayTime * 30
    let yearTime = monthTime * 12

    let ary = item.split(/-|:| /)
    ary[1]--
    let publishTime = new Date(...ary).getTime()
    let historyTime = parseInt(nowTime) - parseInt(publishTime)

    let suffix = historyTime > 0 ? 'ago' : 'left'
    historyTime = historyTime > 0 ? historyTime : -historyTime

    let descTime
    if (historyTime >= yearTime) {
      descTime = parseInt(historyTime / yearTime) + ' years ' + suffix
    } else if (historyTime < yearTime && historyTime >= monthTime) {
      descTime = parseInt(historyTime / monthTime) + ' months ' + suffix
    } else if (historyTime < monthTime && historyTime >= dayTime) {
      descTime = parseInt(historyTime / dayTime) + ' days ' + suffix
    } else if (historyTime < dayTime && historyTime >= hourTime) {
      descTime = parseInt(historyTime / hourTime) + ' hours ' + suffix
    } else if (historyTime < hourTime && historyTime >= minuteTime) {
      descTime = parseInt(historyTime / minuteTime) + ' minutes ' + suffix
    } else {
      descTime = ' few seconds ' + suffix
    }
    return descTime
  },
  formatDateTime (item) {
    let pad = num => {
      return num < 10 ? '0' + num : num
    }
    let tmpDate = new Date(item)
    let year = tmpDate.getFullYear()
    let month = pad(tmpDate.getMonth() + 1)
    let day = pad(tmpDate.getDate())
    let hours = pad(tmpDate.getHours())
    let minutes = pad(tmpDate.getMinutes())
    let seconds = pad(tmpDate.getSeconds())
    return year + '-' + month + '-' + day + ' ' + hours + ':' + minutes + ':' + seconds
  }
}
