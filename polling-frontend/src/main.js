import Vue from 'vue'
import Vuelidate from 'vuelidate'
import Notifications from 'vue-notification'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faCheckCircle, faCheckSquare } from '@fortawesome/free-regular-svg-icons'
import { faHome, faPlus, faUser, faSignInAlt, faUserPlus, faFilter } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import Loading from 'vue-loading-overlay'
import 'bootstrap/dist/js/bootstrap.min'

import App from './App.vue'
import router from './router'
import store from './store'
import Common from './utils/common'

import './filters'
import './repositories'

Vue.component('Loading', Loading)
library.add(faCheckCircle, faCheckSquare, faHome, faPlus, faUser, faSignInAlt, faUserPlus, faFilter)
Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.config.productionTip = false
Vue.use(Vuelidate)
Vue.use(Common)
Vue.use(Notifications)

window.app = new Vue({
  router,
  store,
  render: h => h(App),
  created () {
    let token = localStorage.getItem('token')
    if (token) {
      if (Vue.common.isTokenExpired(token)) {
        localStorage.removeItem('token')
      } else {
        store.dispatch('auth/loadMe')
      }
    }
    document.title = process.env.VUE_APP_TITLE
  }
}).$mount('#app')
