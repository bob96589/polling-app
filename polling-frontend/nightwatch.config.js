const GLOBALS = './globals.js'

module.exports = {
  'selenium': {
    'start_process': false
  },
  'globals_path': GLOBALS,
  'test_settings': {
    'default': {
      'selenium_port': 9515,
      'selenium_host': 'localhost',
      'default_path_prefix': '',
      'desiredCapabilities': {
        'browserName': 'chrome',
        'chromeOptions': {
          'args': ['--no-sandbox']
        },
        'acceptSslCerts': true
      },
      'selenium': {
        'start_process': false
      }
    },
    'headless': {
      'selenium_port': 9515,
      'selenium_host': 'localhost',
      'default_path_prefix': '',
      'desiredCapabilities': {
        'browserName': 'chrome',
        'chromeOptions': {
          'args': ['--no-sandbox', '--headless']
        },
        'acceptSslCerts': true
      },
      'selenium': {
        'start_process': false
      }
    }
  }
}
