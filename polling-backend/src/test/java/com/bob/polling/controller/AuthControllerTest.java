package com.bob.polling.controller;

import java.util.HashMap;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

public class AuthControllerTest extends IntegrationTest {

    @Test
    public void signin() throws Exception {
        String uri = "/auth/signin";
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
                .content("{\"username\": \"bob123\",\"password\": \"123456\"}")).andReturn();
        int status = result.getResponse().getStatus();
        String content = result.getResponse().getContentAsString();

        @SuppressWarnings("unchecked")
        HashMap<String, Object> map = new ObjectMapper().readValue(content, HashMap.class);
        Assert.assertTrue(map.containsKey("tokenType"));
        Assert.assertTrue(map.containsKey("accessToken"));
        Assert.assertEquals("error", 200, status);
    }

    @Test
    public void signup() throws Exception {
        String uri = "/auth/signup";
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
                .content("{\"username\": \"amy\", \"password\": \"password\", \"name\": \"amy_name\"}")).andReturn();
        int status = result.getResponse().getStatus();
        Assert.assertEquals("error", 201, status);
    }

    @Test
    public void refresh() throws Exception {
        String uri = "/auth/refresh";
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri).header(HttpHeaders.AUTHORIZATION, getAuthToken())
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)).andReturn();
        int status = result.getResponse().getStatus();
        String content = result.getResponse().getContentAsString();

        @SuppressWarnings("unchecked")
        HashMap<String, Object> map = new ObjectMapper().readValue(content, HashMap.class);
        Assert.assertTrue(map.containsKey("tokenType"));
        Assert.assertTrue(map.containsKey("accessToken"));
        Assert.assertEquals("error", 200, status);
    }

}
