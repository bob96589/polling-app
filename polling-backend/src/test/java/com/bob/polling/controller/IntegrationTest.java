package com.bob.polling.controller;

import java.util.HashMap;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.bob.polling.PollingServerApplication;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = PollingServerApplication.class)
@WebAppConfiguration
@TestPropertySource(locations = "classpath:application-test.properties")
@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:script.h2.sql")
public abstract class IntegrationTest {

    @Autowired
    private WebApplicationContext webApplicationContext;
    protected MockMvc mvc;

    @Before
    public void setup() {
        mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).apply(SecurityMockMvcConfigurers.springSecurity()).build();
    }

    protected String getAuthToken() throws Exception {
        String uri = "/auth/signin";
        MvcResult signinResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON).content("{\"username\": \"bob123\",\"password\": \"123456\"}")).andReturn();
        String signinContent = signinResult.getResponse().getContentAsString();

        @SuppressWarnings("unchecked")
        HashMap<String, Object> map = new ObjectMapper().readValue(signinContent, HashMap.class);
        String tokenType = (String) map.get("tokenType");
        String accessToken = (String) map.get("accessToken");
        String authToken = tokenType + " " + accessToken;
        return authToken;
    }

}
