package com.bob.polling.controller;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

public class PollControllerTest extends IntegrationTest {

    @Test
    public void getPolls() throws Exception {
        String uri = "/polls";
        MvcResult result = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON)).andReturn();
        int status = result.getResponse().getStatus();

        Assert.assertEquals("error", 200, status);
    }

    @Test
    public void createPoll() throws Exception {
        String uri = "/polls";
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri).header(HttpHeaders.AUTHORIZATION, getAuthToken())
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).content(
                        "{\"question\": \"a or b? \", \"choices\": [{\"text\": \"a\"}, {\"text\": \"b\"}], \"pollLength\": {\"days\": 1, \"hours\": 0}}"))
                .andReturn();
        int status = result.getResponse().getStatus();

        Assert.assertEquals("error", 201, status);
    }

    @Test
    public void getPollById() throws Exception {
        String uri = "/polls/6f2766b7393f4f47af98b91615b8c172";
        MvcResult result = mvc.perform(MockMvcRequestBuilders.get(uri).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andReturn();
        int status = result.getResponse().getStatus();

        Assert.assertEquals("error", 200, status);
    }

    @Test
    public void castVote() throws Exception {
        String uri = "/polls/6f2766b7393f4f47af98b91615b8c172/votes";
        MvcResult result = mvc
                .perform(MockMvcRequestBuilders.post(uri).header(HttpHeaders.AUTHORIZATION, getAuthToken()).contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON).content("{\"choiceId\" : \"8493942681944d12a4b188630406b317\"}"))
                .andReturn();
        int status = result.getResponse().getStatus();

        Assert.assertTrue("Sorry! This Poll has already expired".equals(result.getResolvedException().getMessage()));
        Assert.assertEquals("error", 400, status);
    }

}
