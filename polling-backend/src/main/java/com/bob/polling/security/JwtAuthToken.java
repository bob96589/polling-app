package com.bob.polling.security;

import java.util.Collection;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class JwtAuthToken extends AbstractAuthenticationToken {

    private static final long serialVersionUID = 1L;
    private String token;
    private UserDetails userPrincipal;

    public JwtAuthToken(String token) {
        super(null);
        this.token = token;
        setAuthenticated(false);
    }

    public JwtAuthToken(String token, Collection<? extends GrantedAuthority> authorities, UserDetails userPrinciple) {
        super(authorities);
        this.token = token;
        this.userPrincipal = userPrinciple;
        setAuthenticated(true);
    }

    @Override
    public Object getCredentials() {
        return null;
    }

    @Override
    public Object getPrincipal() {
        return userPrincipal;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
