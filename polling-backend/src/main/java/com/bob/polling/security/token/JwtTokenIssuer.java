package com.bob.polling.security.token;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JwtTokenIssuer {

    @Value("${app.jwt.secret}")
    private String secret;
    @Value("${app.jwt.expirationInMs}")
    private int expirationInMs;
    @Value("${app.jwt.issuer}")
    private String issuer;
    @Value("${app.jwt.audience}")
    private String audience;
    @Value("${app.jwt.refreshLimit}")
    private int refreshLimit;
    @Value("${app.jwt.claimNames.refreshCount}")
    private String refreshCountClaim;
    @Value("${app.jwt.claimNames.refreshLimit}")
    private String refreshLimitClaim;

    public String issueToken(TokenInfo tokenInfo) {

        return Jwts.builder().setId(tokenInfo.getId()).setIssuer(issuer).setAudience(audience).setSubject(tokenInfo.getUsername())
                .setIssuedAt(tokenInfo.getIssuedDate()).setExpiration(tokenInfo.getExpirationDate())
                .claim(refreshCountClaim, tokenInfo.getRefreshCount()).claim(refreshLimitClaim, refreshLimit)
                .signWith(SignatureAlgorithm.HS256, secret).compact();
    }

}
