package com.bob.polling.security.token;

import java.util.Date;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.bob.polling.exception.InvalidAuthenticationTokenException;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.InvalidClaimException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.UnsupportedJwtException;

@Component
public class JwtTokenParser {

    @Value("${app.jwt.secret}")
    private String secret;
    @Value("${app.jwt.expirationInMs}")
    private int expirationInMs;
    @Value("${app.jwt.issuer}")
    private String issuer;
    @Value("${app.jwt.audience}")
    private String audience;
    @Value("${app.jwt.clockSkew}")
    private int clockSkew;
    @Value("${app.jwt.claimNames.refreshCount}")
    private String refreshCount;
    @Value("${app.jwt.claimNames.refreshLimit}")
    private String refreshLimit;

    public TokenInfo parseToken(String token) {

        try {

            Claims claims = Jwts.parser().setSigningKey(secret).requireAudience(audience).setAllowedClockSkewSeconds(clockSkew).parseClaimsJws(token)
                    .getBody();

            return new TokenInfo.Builder().withId(extractTokenIdFromClaims(claims)).withUsername(extractUsernameFromClaims(claims))
                    .withIssuedDate(extractIssuedDateFromClaims(claims)).withExpirationDate(extractExpirationDateFromClaims(claims))
                    .withRefreshCount(extractRefreshCountFromClaims(claims)).withRefreshLimit(extractRefreshLimitFromClaims(claims)).build();

        } catch (UnsupportedJwtException | MalformedJwtException | IllegalArgumentException e) {
            throw new InvalidAuthenticationTokenException("Invalid token", e);
        } catch (ExpiredJwtException e) {
            throw new InvalidAuthenticationTokenException("Expired token", e);
        } catch (InvalidClaimException e) {
            throw new InvalidAuthenticationTokenException("Invalid value for claim \"" + e.getClaimName() + "\"", e);
        } catch (Exception e) {
            throw new InvalidAuthenticationTokenException("Invalid token, Exception: ", e);
        }
    }

    private String extractTokenIdFromClaims(@NotNull Claims claims) {
        return (String) claims.get(Claims.ID);
    }

    private String extractUsernameFromClaims(@NotNull Claims claims) {
        return claims.getSubject();
    }

    private Date extractIssuedDateFromClaims(@NotNull Claims claims) {
        return claims.getIssuedAt();
    }

    private Date extractExpirationDateFromClaims(@NotNull Claims claims) {
        return claims.getExpiration();
    }

    private int extractRefreshCountFromClaims(@NotNull Claims claims) {
        return (int) claims.get(refreshCount);
    }

    private int extractRefreshLimitFromClaims(@NotNull Claims claims) {
        return (int) claims.get(refreshLimit);
    }

}
