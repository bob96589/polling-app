package com.bob.polling.security.service;

import java.util.Date;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.bob.polling.exception.InvalidAuthenticationTokenException;
import com.bob.polling.security.token.JwtTokenIssuer;
import com.bob.polling.security.token.JwtTokenParser;
import com.bob.polling.security.token.TokenInfo;

@Component
public class JwtTokenService {

    @Value("${app.jwt.expirationInMs}")
    private int expirationInMs;
    @Value("${app.jwt.secret}")
    private String secret;
    @Autowired
    private JwtTokenIssuer jwtTokenIssuer;
    @Autowired
    private JwtTokenParser jwtTokenParser;

    public String generateToken(String username) {
        Date issuedDate = new Date();
        Date expirationDate = new Date(issuedDate.getTime() + expirationInMs);
        TokenInfo newTokenInfo = new TokenInfo.Builder().withId(generateTokenIdentifier()).withUsername(username).withIssuedDate(issuedDate)
                .withExpirationDate(expirationDate).withRefreshCount(0).build();
        return jwtTokenIssuer.issueToken(newTokenInfo);
    }

    public String refreshToken(TokenInfo tokenInfo) {
        if (!tokenInfo.isEligibleForRefreshment()) {
            throw new InvalidAuthenticationTokenException("This token cannot be refreshed");
        }
        Date issuedDate = new Date();
        Date expirationDate = new Date(issuedDate.getTime() + expirationInMs);
        TokenInfo newTokenInfo = new TokenInfo.Builder().withId(generateTokenIdentifier()).withUsername(tokenInfo.getUsername())
                .withIssuedDate(issuedDate).withExpirationDate(expirationDate).withRefreshCount(tokenInfo.getRefreshCount() + 1).build();
        return jwtTokenIssuer.issueToken(newTokenInfo);
    }

    public TokenInfo parseToken(String token) {
        return jwtTokenParser.parseToken(token);
    }

    private String generateTokenIdentifier() {
        return UUID.randomUUID().toString();
    }
}