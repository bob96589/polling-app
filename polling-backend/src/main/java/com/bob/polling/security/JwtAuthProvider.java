package com.bob.polling.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import com.bob.polling.security.service.CustomUserDetailsService;
import com.bob.polling.security.service.JwtTokenService;
import com.bob.polling.security.token.TokenInfo;

@Component
public class JwtAuthProvider implements AuthenticationProvider {

    @Autowired
    private JwtTokenService jwtTokenService;
    @Autowired
    private CustomUserDetailsService customUserDetailsService;

    @Override
    public Authentication authenticate(Authentication authentication) {
        JwtAuthToken jwtAuthenticationToken = (JwtAuthToken) authentication;
        String token = jwtAuthenticationToken.getToken();
        TokenInfo tokenInfo = jwtTokenService.parseToken(token);
        return createSuccessAuthentication(token, tokenInfo);
    }

    private Authentication createSuccessAuthentication(String token, TokenInfo tokenInfo) {
        UserDetails userDetails = customUserDetailsService.loadUserByUsername(tokenInfo.getUsername());
        JwtAuthToken jwtAuthToken = new JwtAuthToken(token, userDetails.getAuthorities(), userDetails);
        jwtAuthToken.setDetails(tokenInfo);
        return jwtAuthToken;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return (JwtAuthToken.class.isAssignableFrom(authentication));
    }

}
