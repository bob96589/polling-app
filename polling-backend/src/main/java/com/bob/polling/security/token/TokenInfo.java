package com.bob.polling.security.token;

import java.util.Date;

public class TokenInfo {

    private final String id;
    private final String username;
    private final Date issuedDate;
    private final Date expirationDate;
    private final int refreshCount;
    private final int refreshLimit;

    public TokenInfo(String id, String username, Date issuedDate, Date expirationDate, int refreshCount, int refreshLimit) {
        super();
        this.id = id;
        this.username = username;
        this.issuedDate = issuedDate;
        this.expirationDate = expirationDate;
        this.refreshCount = refreshCount;
        this.refreshLimit = refreshLimit;
    }

    public String getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public Date getIssuedDate() {
        return issuedDate;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public int getRefreshCount() {
        return refreshCount;
    }

    public int getRefreshLimit() {
        return refreshLimit;
    }

    public static class Builder {
        private String id;
        private String username;
        private Date issuedDate;
        private Date expirationDate;
        private int refreshCount;
        private int refreshLimit;

        public Builder withId(String id) {
            this.id = id;
            return this;
        }

        public Builder withUsername(String username) {
            this.username = username;
            return this;
        }

        public Builder withIssuedDate(Date issuedDate) {
            this.issuedDate = issuedDate;
            return this;
        }

        public Builder withExpirationDate(Date expirationDate) {
            this.expirationDate = expirationDate;
            return this;
        }

        public Builder withRefreshCount(int refreshCount) {
            this.refreshCount = refreshCount;
            return this;
        }

        public Builder withRefreshLimit(int refreshLimit) {
            this.refreshLimit = refreshLimit;
            return this;
        }

        public TokenInfo build() {
            return new TokenInfo(id, username, issuedDate, expirationDate, refreshCount, refreshLimit);
        }
    }

    public boolean isEligibleForRefreshment() {
        return refreshCount < refreshLimit;
    }

}
