package com.bob.polling;

import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * <pre>
 * Spring Boot entry point
 * </pre>
 * 
 * @since 2019-03-15
 * @author bob
 * @version
 *          <ul>
 *          <li>2019-03-15,bob,new
 *          </ul>
 */
@SpringBootApplication
@EnableTransactionManagement
public class PollingServerApplication {

    private static final Logger LOGGER = LogManager.getLogger(PollingServerApplication.class);

    @PostConstruct
    void init() {
        TimeZone.setDefault(TimeZone.getTimeZone("Asia/Taipei"));
    }

    public static void main(String[] args) {
        SpringApplication.run(PollingServerApplication.class, args);
        LOGGER.info("Info level log message");
        LOGGER.debug("Debug level log message");
        LOGGER.error("Error level log message");
    }

}
