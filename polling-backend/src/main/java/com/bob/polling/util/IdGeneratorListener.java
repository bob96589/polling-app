package com.bob.polling.util;

import java.util.UUID;

import javax.persistence.PrePersist;

import com.bob.polling.model.DataObject;

public class IdGeneratorListener {
    @PrePersist
    public <T extends DataObject> void perPersist(T entity) {
        String uuid = UUID.randomUUID().toString().replace("-", "");
        entity.setId(uuid);
    }
}
