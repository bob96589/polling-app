package com.bob.polling.util;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.bob.polling.model.Poll;
import com.bob.polling.model.User;
import com.bob.polling.payload.ChoiceResponse;
import com.bob.polling.payload.PollResponse;
import com.bob.polling.payload.UserSummary;

public class ModelMapper {
    public static PollResponse mapPollToPollResponse(Poll poll, Map<String, Long> choiceVotesMap, User creator, String userVote) {
        // userVote: choice id
        PollResponse pollResponse = new PollResponse();
        pollResponse.setId(poll.getId());
        pollResponse.setQuestion(poll.getQuestion());
        pollResponse.setCreateTime(poll.getCreateTime());
        pollResponse.setExpireTime(poll.getExpireTime());
        pollResponse.setExpired(poll.getExpireTime().before(new Date()));

        List<ChoiceResponse> choiceResponses = poll.getChoices().stream().map(choice -> {
            ChoiceResponse choiceResponse = new ChoiceResponse();
            choiceResponse.setId(choice.getId());
            choiceResponse.setText(choice.getText());

            if (choiceVotesMap.containsKey(choice.getId())) {
                choiceResponse.setVoteCount(choiceVotesMap.get(choice.getId()));
            } else {
                choiceResponse.setVoteCount(0);
            }
            return choiceResponse;
        }).collect(Collectors.toList());

        pollResponse.setChoices(choiceResponses);

        UserSummary creatorSummary = new UserSummary();
        creatorSummary.setId(creator.getId());
        creatorSummary.setUsername(creator.getUsername());
        creatorSummary.setName(creator.getName());
        pollResponse.setCreator(creatorSummary);

        if (userVote != null) {
            pollResponse.setSelectedChoiceId(userVote);
        }

        long totalVotes = pollResponse.getChoices().stream().mapToLong(ChoiceResponse::getVoteCount).sum();
        pollResponse.setTotalVotes(totalVotes);

        return pollResponse;
    }

}
