package com.bob.polling.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bob.polling.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, String> {

    List<User> findByIdIn(List<String> creatorIds);

    Optional<User> findByUsername(String username);

    Boolean existsByUsername(String username);

}
