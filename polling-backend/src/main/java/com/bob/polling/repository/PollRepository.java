package com.bob.polling.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.bob.polling.model.Poll;

@Repository
public interface PollRepository extends JpaRepository<Poll, String>, JpaSpecificationExecutor<Poll> {

    long countByCreator(String creator);

    Page<Poll> findByCreator(String id, Pageable pageable);

    List<Poll> findByIdIn(List<String> pollIds, Sort sort);

    Page<Poll> findByIdIn(List<String> pollIds, Pageable pageable);

}
