package com.bob.polling.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bob.polling.model.Choice;

@Repository
public interface ChoiceRepository extends JpaRepository<Choice, String> {

    void deleteByPollId(String pollId);

}
