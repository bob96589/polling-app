package com.bob.polling.repository;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bob.polling.dto.ChoiceVoteCount;
import com.bob.polling.model.Vote;

@Repository
public interface VoteRepository extends JpaRepository<Vote, String> {

    @Query(name = "VoteRepository.countByPollIdGroupByChoiceId")
    List<ChoiceVoteCount> countByPollIdGroupByChoiceId(@Param("pollId") String pollId);

    @Query(name = "VoteRepository.countByPollIdInGroupByChoiceId")
    List<ChoiceVoteCount> countByPollIdInGroupByChoiceId(List<String> pollIds);

    @Query(name = "VoteRepository.countByUserId")
    long countByUserId(@Param("userId") String userId);

    @Query(name = "VoteRepository.findVotedPollIdsByUserId")
    List<String> findVotedPollIdsByUserId(@Param("userId") String userId);

    @Query(name = "VoteRepository.findByUserIdAndPollIdIn")
    List<Map<String, String>> findByUserIdAndPollIdIn(@Param("userId") String id, @Param("pollIds") List<String> pollIds);

    @Query(name = "VoteRepository.findByUserIdAndPollId")
    Map<String, String> findByUserIdAndPollId(@Param("userId") String id, @Param("pollId") String pollId);

    List<Vote> findByChoiceId(String choiceId);

    @Modifying
    @Query(name = "VoteRepository.deleteByPollId")
    void deleteByPollId(@Param("pollId") String pollId);

}
