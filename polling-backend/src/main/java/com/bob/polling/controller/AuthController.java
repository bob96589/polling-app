package com.bob.polling.controller;

import java.net.URI;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.bob.polling.model.User;
import com.bob.polling.payload.ApiResponse;
import com.bob.polling.payload.JwtAuthenticationResponse;
import com.bob.polling.payload.LoginRequest;
import com.bob.polling.payload.SignUpRequest;
import com.bob.polling.security.CurrentUser;
import com.bob.polling.security.JwtAuthToken;
import com.bob.polling.security.UserPrincipal;
import com.bob.polling.security.service.JwtTokenService;
import com.bob.polling.security.token.TokenInfo;
import com.bob.polling.service.UserService;

@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    UserService userService;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    JwtTokenService jwtTokenService;

    private static final Logger logger = LogManager.getLogger(AuthController.class);

    @PostMapping("/signin")
    public ResponseEntity<JwtAuthenticationResponse> signin(@Valid @RequestBody LoginRequest loginRequest) {
        logger.debug("1111111111111111111111111111111111111111");
        Authentication authentication = authenticationManager
                .authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
        String jwt = jwtTokenService.generateToken(loginRequest.getUsername());
        return ResponseEntity.ok(new JwtAuthenticationResponse(jwt));
    }

    @PostMapping("/signup")
    public ResponseEntity<ApiResponse> signup(@Valid @RequestBody SignUpRequest signUpRequest) {
        if (userService.existsByUsername(signUpRequest.getUsername())) {
            return ResponseEntity.badRequest().body(new ApiResponse(false, "Username is already taken!"));
        }

        // Creating user's account
        User user = new User();
        user.setName(signUpRequest.getName());
        user.setUsername(signUpRequest.getUsername());
        user.setPassword(passwordEncoder.encode(signUpRequest.getPassword()));
        user.setRole("User");

        User result = userService.save(user);
        URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/users/{username}").buildAndExpand(result.getUsername()).toUri();
        return ResponseEntity.created(location).body(new ApiResponse(true, "User registered successfully"));
    }

    @PreAuthorize("isAuthenticated()")
    @PostMapping("/refresh")
    public ResponseEntity<JwtAuthenticationResponse> refresh(@CurrentUser UserPrincipal currentUser) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        TokenInfo tokenInfo = (TokenInfo) ((JwtAuthToken) auth).getDetails();

        // get old token first
        String jwt = jwtTokenService.refreshToken(tokenInfo);
        return ResponseEntity.ok(new JwtAuthenticationResponse(jwt));
    }
}
