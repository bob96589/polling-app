package com.bob.polling.controller;

import java.net.URI;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.bob.polling.model.Poll;
import com.bob.polling.payload.ApiResponse;
import com.bob.polling.payload.PagedResponse;
import com.bob.polling.payload.PollRequest;
import com.bob.polling.payload.PollResponse;
import com.bob.polling.payload.VoteRequest;
import com.bob.polling.security.CurrentUser;
import com.bob.polling.security.UserPrincipal;
import com.bob.polling.service.PollService;
import com.bob.polling.util.AppConstants;

@RestController
@RequestMapping("/polls")
public class PollController {

    @Autowired
    private PollService pollService;

    @GetMapping
    public ResponseEntity<PagedResponse<PollResponse>> getPolls(@CurrentUser UserPrincipal currentUser,
            @RequestParam(value = "listType", defaultValue = "all") String listType,
            @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
            @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size) {
        return ResponseEntity.ok().body(pollService.getPolls(currentUser, listType, page, size));
    }

    @PreAuthorize("hasAnyAuthority('User', 'Admin')")
    @PostMapping
    public ResponseEntity<ApiResponse> createPoll(@Valid @RequestBody PollRequest pollRequest) {
        Poll poll = pollService.createPoll(pollRequest);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{pollId}").buildAndExpand(poll.getId()).toUri();
        return ResponseEntity.created(location).body(new ApiResponse(true, "Poll Created Successfully"));
    }

    @GetMapping("/{pollId}")
    public ResponseEntity<PollResponse> getPollById(@CurrentUser UserPrincipal currentUser, @PathVariable String pollId) {
        return ResponseEntity.ok().body(pollService.getPollById(currentUser, pollId));
    }

    @PreAuthorize("hasAnyAuthority('User', 'Admin')")
    @PostMapping("/{pollId}/votes")
    public ResponseEntity<PollResponse> castVote(@CurrentUser UserPrincipal currentUser, @PathVariable String pollId,
            @Valid @RequestBody VoteRequest voteRequest) {
        // TODO: check if duplicated vote
        return ResponseEntity.ok().body(pollService.castVoteAndGetUpdatedPoll(currentUser, pollId, voteRequest));
    }

    @PreAuthorize("hasAuthority('Admin')")
    @DeleteMapping("/{pollId}")
    public ResponseEntity<ApiResponse> deletePoll(@PathVariable String pollId) {
        pollService.deletePoll(pollId);
        return ResponseEntity.ok().body(new ApiResponse(true, "Poll Deleted Successfully"));
    }

}
