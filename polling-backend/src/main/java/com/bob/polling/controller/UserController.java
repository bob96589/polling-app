package com.bob.polling.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bob.polling.payload.PagedResponse;
import com.bob.polling.payload.PollResponse;
import com.bob.polling.payload.UserProfile;
import com.bob.polling.payload.UserSummary;
import com.bob.polling.security.CurrentUser;
import com.bob.polling.security.UserPrincipal;
import com.bob.polling.service.PollService;
import com.bob.polling.service.UserService;
import com.bob.polling.util.AppConstants;

@RestController
@RequestMapping("")
public class UserController {

    @Autowired
    private UserService userService;
    @Autowired
    private PollService pollService;

    @PreAuthorize("isAuthenticated()")
    @GetMapping("/user/me")
    public ResponseEntity<UserSummary> getCurrentUser(@CurrentUser UserPrincipal currentUser) {
        UserSummary userSummary = new UserSummary();
        userSummary.setId(currentUser.getId());
        userSummary.setName(currentUser.getName());
        userSummary.setUsername(currentUser.getUsername());
        StringBuilder sb = new StringBuilder();
        currentUser.getAuthorities().forEach(authoritiy -> {
            boolean first = sb.length() == 0;
            sb.append(authoritiy.getAuthority());
            if (!first) {
                sb.append(",");
            }
        });
        userSummary.setAuthorities(sb.toString());
        return ResponseEntity.ok().body(userSummary);
    }

    @GetMapping("/users")
    public ResponseEntity<List<UserSummary>> getUsersByChoiceId(@RequestParam(value = "choiceId") String choiceId) {
        return ResponseEntity.ok().body(userService.findUserByChoiceId(choiceId));
    }

    @PreAuthorize("isAuthenticated()")
    @GetMapping("/users/{username}")
    public ResponseEntity<UserProfile> getUserProfile(@PathVariable(value = "username") String username) {
        return ResponseEntity.ok().body(userService.findUserProfileByUsername(username));
    }

    @PreAuthorize("isAuthenticated()")
    @GetMapping("/users/{username}/polls")
    public ResponseEntity<PagedResponse<PollResponse>> getPollsCreatedBy(@CurrentUser UserPrincipal currentUser,
            @PathVariable(value = "username") String username,
            @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
            @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size) {
        return ResponseEntity.ok().body(pollService.getPollsByCreator(currentUser, username, page, size));
    }

    @PreAuthorize("isAuthenticated()")
    @GetMapping("/users/{username}/votes")
    public ResponseEntity<PagedResponse<PollResponse>> getPollsVotedBy(@CurrentUser UserPrincipal currentUser,
            @PathVariable(value = "username") String username,
            @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
            @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size) {
        return ResponseEntity.ok().body(pollService.getPollsVotedBy(currentUser, username, page, size));
    }

}
