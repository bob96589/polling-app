package com.bob.polling.dto;

public interface ChoiceVoteCount {
    String getChoiceId();

    Long getVoteCnt();
}
