package com.bob.polling.exception;

public class InvalidAuthenticationTokenException extends RuntimeException {

    private static final long serialVersionUID = 6585989919036489588L;

    public InvalidAuthenticationTokenException(String message) {
        super(message);
    }

    public InvalidAuthenticationTokenException(String message, Throwable cause) {
        super(message, cause);
    }
}