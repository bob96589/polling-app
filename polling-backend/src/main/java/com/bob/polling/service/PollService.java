package com.bob.polling.service;

import com.bob.polling.model.Poll;
import com.bob.polling.payload.PagedResponse;
import com.bob.polling.payload.PollRequest;
import com.bob.polling.payload.PollResponse;
import com.bob.polling.payload.VoteRequest;
import com.bob.polling.security.UserPrincipal;

public interface PollService {

    Poll createPoll(PollRequest pollRequest);

    PollResponse getPollById(UserPrincipal currentUser, String pollId);

    PollResponse castVoteAndGetUpdatedPoll(UserPrincipal currentUser, String pollId, VoteRequest voteRequest);

    PagedResponse<PollResponse> getPolls(UserPrincipal currentUser, String listType, int page, int size);

    PagedResponse<PollResponse> getPollsByCreator(UserPrincipal currentUser, String username, int page, int size);

    PagedResponse<PollResponse> getPollsVotedBy(UserPrincipal currentUser, String username, int page, int size);

    void deletePoll(String pollId);

}
