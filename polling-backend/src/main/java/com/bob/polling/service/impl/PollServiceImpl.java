package com.bob.polling.service.impl;

import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bob.polling.dto.ChoiceVoteCount;
import com.bob.polling.exception.BadRequestException;
import com.bob.polling.exception.ResourceNotFoundException;
import com.bob.polling.model.Choice;
import com.bob.polling.model.Poll;
import com.bob.polling.model.User;
import com.bob.polling.model.Vote;
import com.bob.polling.payload.PagedResponse;
import com.bob.polling.payload.PollRequest;
import com.bob.polling.payload.PollResponse;
import com.bob.polling.payload.VoteRequest;
import com.bob.polling.repository.ChoiceRepository;
import com.bob.polling.repository.PollRepository;
import com.bob.polling.repository.UserRepository;
import com.bob.polling.repository.VoteRepository;
import com.bob.polling.security.UserPrincipal;
import com.bob.polling.service.PollService;
import com.bob.polling.util.AppConstants;
import com.bob.polling.util.ModelMapper;

@Service
public class PollServiceImpl implements PollService {

    @Autowired
    private PollRepository pollRepository;
    @Autowired
    private ChoiceRepository choiceRepository;
    @Autowired
    private VoteRepository voteRepository;
    @Autowired
    private UserRepository userRepository;

    @Override
    @Transactional
    public Poll createPoll(PollRequest pollRequest) {
        Poll poll = new Poll();
        poll.setQuestion(pollRequest.getQuestion());

        pollRequest.getChoices().forEach(choiceRequest -> {
            Choice choice = new Choice();
            choice.setText(choiceRequest.getText());
            poll.addChoice(choice);
        });

        Calendar cal = Calendar.getInstance(); // creates calendar
        cal.setTime(new Date()); // sets calendar time/date
        cal.add(Calendar.DAY_OF_YEAR, pollRequest.getPollLength().getDays());
        cal.add(Calendar.HOUR_OF_DAY, pollRequest.getPollLength().getHours()); // adds one hour
        poll.setExpireTime(cal.getTime());

        return pollRepository.save(poll);
    }

    @Override
    public PollResponse getPollById(UserPrincipal currentUser, String pollId) {
        Poll poll = pollRepository.findById(pollId).orElseThrow(() -> new ResourceNotFoundException("Poll", "id", pollId));

        // Retrieve Vote Counts of every choice belonging to the current poll
        List<ChoiceVoteCount> votes = voteRepository.countByPollIdGroupByChoiceId(pollId);
        Map<String, Long> choiceVotesMap = votes.stream().collect(Collectors.toMap(ChoiceVoteCount::getChoiceId, ChoiceVoteCount::getVoteCnt));

        // Retrieve poll creator details
        String creator = poll.getCreator();
        User user = userRepository.findById(creator).orElseThrow(() -> new ResourceNotFoundException("User", "id", creator));

        // Retrieve vote done by logged in user
        Map<String, String> userVote = null;
        if (currentUser != null) {
            userVote = voteRepository.findByUserIdAndPollId(currentUser.getId(), pollId);
        }
        return ModelMapper.mapPollToPollResponse(poll, choiceVotesMap, user, userVote != null ? userVote.get("CHOICE_ID") : null);

    }

    @Override
    @Transactional
    public PollResponse castVoteAndGetUpdatedPoll(UserPrincipal currentUser, String pollId, VoteRequest voteRequest) {
        Poll poll = pollRepository.findById(pollId).orElseThrow(() -> new ResourceNotFoundException("Poll", "id", pollId));

        if (poll.getExpireTime().before(new Date())) {
            throw new BadRequestException("Sorry! This Poll has already expired");
        }

        User user = userRepository.getOne(currentUser.getId());

        Choice selectedChoice = poll.getChoices().stream().filter(choice -> choice.getId().equals(voteRequest.getChoiceId())).findFirst()
                .orElseThrow(() -> new ResourceNotFoundException("Choice", "id", voteRequest.getChoiceId()));

        Vote vote = new Vote();
        vote.setUser(user);
        vote.setChoice(selectedChoice);

        try {
            vote = voteRepository.save(vote);
        } catch (DataIntegrityViolationException ex) {
            throw new BadRequestException("Sorry! You have already cast your vote in this poll");
        }

        // -- Vote Saved, Return the updated Poll Response now --
        // Retrieve Vote Counts of every choice belonging to the current polls
        List<ChoiceVoteCount> votes = voteRepository.countByPollIdGroupByChoiceId(pollId);
        Map<String, Long> choiceVotesMap = votes.stream().collect(Collectors.toMap(ChoiceVoteCount::getChoiceId, ChoiceVoteCount::getVoteCnt));
        // Retrieve poll creator details
        User creator = userRepository.findById(poll.getCreator()).orElseThrow(() -> new ResourceNotFoundException("User", "id", poll.getCreator()));

        return ModelMapper.mapPollToPollResponse(poll, choiceVotesMap, creator, vote.getChoice().getId());
    }

    @Override
    public PagedResponse<PollResponse> getPolls(UserPrincipal currentUser, String listType, int page, int size) {
        validatePageNumberAndSize(page, size);
        // Retrieve Polls
        Pageable pageable = PageRequest.of(page, size, Sort.Direction.DESC, "createTime");
        Page<Poll> polls = null;
        if ("ongoing".equals(listType)) {
            polls = pollRepository.findAll(notExpiredSpec(), pageable);
        } else if ("closed".equals(listType)) {
            polls = pollRepository.findAll(expiredSpec(), pageable);
        } else if ("all".equals(listType)) {
            polls = pollRepository.findAll(pageable);
        }
        return toPagedResponse(polls, currentUser);
    }

    public static Specification<Poll> notExpiredSpec() {
        return new Specification<Poll>() {
            @Override
            public Predicate toPredicate(Root<Poll> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                return cb.greaterThanOrEqualTo(root.get("expireTime"), new Date());
            }
        };
    }

    public static Specification<Poll> expiredSpec() {
        return new Specification<Poll>() {
            @Override
            public Predicate toPredicate(Root<Poll> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                return cb.lessThan(root.get("expireTime"), new Date());
            }
        };
    }

    @Override
    public PagedResponse<PollResponse> getPollsByCreator(UserPrincipal currentUser, String username, int page, int size) {
        validatePageNumberAndSize(page, size);
        User user = userRepository.findByUsername(username).orElseThrow(() -> new ResourceNotFoundException("User", "username", username));
        // Retrieve all polls created by the given username
        Pageable pageable = PageRequest.of(page, size, Sort.Direction.DESC, "createTime");
        Page<Poll> polls = pollRepository.findByCreator(user.getId(), pageable);
        return toPagedResponse(polls, currentUser);
    }

    @Override
    public PagedResponse<PollResponse> getPollsVotedBy(UserPrincipal currentUser, String username, int page, int size) {
        validatePageNumberAndSize(page, size);
        User user = userRepository.findByUsername(username).orElseThrow(() -> new ResourceNotFoundException("User", "username", username));
        // Retrieve all pollIds in which the given username has voted
        List<String> userVotedPollIds = voteRepository.findVotedPollIdsByUserId(user.getId());
        Pageable pageable = PageRequest.of(page, size, Sort.Direction.DESC, "createTime");
        Page<Poll> polls = pollRepository.findByIdIn(userVotedPollIds, pageable);
        return toPagedResponse(polls, currentUser);
    }

    private PagedResponse<PollResponse> toPagedResponse(Page<Poll> polls, UserPrincipal currentUser) {
        if (polls.getNumberOfElements() == 0) {
            return new PagedResponse<>(Collections.emptyList(), polls.getNumber(), polls.getSize(), polls.getTotalElements(), polls.getTotalPages(),
                    polls.isLast());
        }
        // Map Polls to PollResponses containing vote counts and poll creator details
        List<String> pollIds = polls.map(Poll::getId).getContent();
        Map<String, Long> choiceVoteCountMap = getChoiceVoteCountMap(pollIds);
        Map<String, String> pollUserVoteMap = getPollUserVoteMap(currentUser, pollIds);
        Map<String, User> creatorMap = getPollCreatorMap(polls.getContent());
        List<PollResponse> pollResponses = polls.map(poll -> ModelMapper.mapPollToPollResponse(poll, choiceVoteCountMap,
                creatorMap.get(poll.getCreator()), pollUserVoteMap == null ? null : pollUserVoteMap.getOrDefault(poll.getId(), null))).getContent();
        return new PagedResponse<>(pollResponses, polls.getNumber(), polls.getSize(), polls.getTotalElements(), polls.getTotalPages(),
                polls.isLast());
    }

    private Map<String, Long> getChoiceVoteCountMap(List<String> pollIds) {
        // Retrieve Vote Counts of every Choice belonging to the given pollIds
        List<ChoiceVoteCount> votes = voteRepository.countByPollIdInGroupByChoiceId(pollIds);
        return votes.stream().collect(Collectors.toMap(ChoiceVoteCount::getChoiceId, ChoiceVoteCount::getVoteCnt));
    }

    private Map<String, String> getPollUserVoteMap(UserPrincipal currentUser, List<String> pollIds) {
        // Retrieve Votes done by the logged in user to the given pollIds
        Map<String, String> pollUserVoteMap = null;
        if (currentUser != null) {
            List<Map<String, String>> list = voteRepository.findByUserIdAndPollIdIn(currentUser.getId(), pollIds);
            pollUserVoteMap = list.stream()
                    .collect(Collectors.<Map<String, String>, String, String> toMap(map -> map.get("POLL_ID"), map -> map.get("CHOICE_ID")));
        }
        return pollUserVoteMap;
    }

    private Map<String, User> getPollCreatorMap(List<Poll> polls) {
        // Get Poll Creator details of the given list of polls
        List<String> creatorIds = polls.stream().map(Poll::getCreator).distinct().collect(Collectors.toList());
        List<User> creators = userRepository.findByIdIn(creatorIds);
        return creators.stream().collect(Collectors.toMap(User::getId, Function.identity()));
    }

    private void validatePageNumberAndSize(int page, int size) {
        if (page < 0) {
            throw new BadRequestException("Page number cannot be less than zero.");
        }
        if (size > AppConstants.MAX_PAGE_SIZE) {
            throw new BadRequestException("Page size must not be greater than " + AppConstants.MAX_PAGE_SIZE);
        }
    }

    @Override
    @Transactional
    public void deletePoll(String pollId) {
        voteRepository.deleteByPollId(pollId);
        choiceRepository.deleteByPollId(pollId);
        pollRepository.deleteById(pollId);
    }

}
