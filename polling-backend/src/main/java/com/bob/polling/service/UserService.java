package com.bob.polling.service;

import java.util.List;

import com.bob.polling.model.User;
import com.bob.polling.payload.UserProfile;
import com.bob.polling.payload.UserSummary;

public interface UserService {

    UserProfile findUserProfileByUsername(String username);

    Boolean existsByUsername(String username);

    User save(User user);

    List<UserSummary> findUserByChoiceId(String choiceId);

}
