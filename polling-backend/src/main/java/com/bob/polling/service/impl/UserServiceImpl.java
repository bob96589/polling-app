package com.bob.polling.service.impl;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bob.polling.exception.ResourceNotFoundException;
import com.bob.polling.model.User;
import com.bob.polling.model.Vote;
import com.bob.polling.payload.UserProfile;
import com.bob.polling.payload.UserSummary;
import com.bob.polling.repository.PollRepository;
import com.bob.polling.repository.UserRepository;
import com.bob.polling.repository.VoteRepository;
import com.bob.polling.service.UserService;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private PollRepository pollRepository;
    @Autowired
    private VoteRepository voteRepository;
    @Autowired
    private UserRepository userRepository;

    @Override
    public UserProfile findUserProfileByUsername(String username) {
        User user = userRepository.findByUsername(username).orElseThrow(() -> new ResourceNotFoundException("User", "username", username));

        long pollCount = pollRepository.countByCreator(user.getId());
        long voteCount = voteRepository.countByUserId(user.getId());

        UserProfile userProfile = new UserProfile();
        userProfile.setId(user.getId());
        userProfile.setUsername(user.getUsername());
        userProfile.setName(user.getName());
        userProfile.setCreateTime(user.getCreateTime());
        userProfile.setPollCount(pollCount);
        userProfile.setVoteCount(voteCount);
        return userProfile;
    }

    @Override
    public Boolean existsByUsername(String username) {
        return userRepository.existsByUsername(username);
    }

    @Override
    @Transactional
    public User save(User user) {
        return userRepository.save(user);
    }

    @Override
    public List<UserSummary> findUserByChoiceId(String choiceId) {
        List<Vote> list = voteRepository.findByChoiceId(choiceId);
        Function<Vote, UserSummary> toUserSummary = new Function<Vote, UserSummary>() {
            public UserSummary apply(Vote v) {
                User user = v.getUser();
                UserSummary userSummary = new UserSummary();
                userSummary.setId(user.getId());
                userSummary.setUsername(user.getUsername());
                userSummary.setName(user.getName());
                return userSummary;
            }
        };
        List<UserSummary> resultList = list.stream().map(toUserSummary).collect(Collectors.<UserSummary> toList());
        return resultList;
    }

}
