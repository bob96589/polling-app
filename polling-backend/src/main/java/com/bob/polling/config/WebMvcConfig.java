package com.bob.polling.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * <pre>
 * Allow Cross-Origin Resource Sharing
 * </pre>
 * 
 * @since 2019-03-15
 * @author bob
 * @version
 *          <ul>
 *          <li>2019-03-15,bob,new
 *          </ul>
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    private final static long MAX_AGE_SECS = 3600;

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**").allowedOrigins("*").allowedMethods("HEAD", "OPTIONS", "GET", "POST", "PUT", "PATCH", "DELETE")
                .maxAge(MAX_AGE_SECS);
    }
}
