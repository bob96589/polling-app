package com.bob.polling.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.annotation.AuthenticationPrincipal;

import com.bob.polling.security.CurrentUser;

import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * <pre>
 * Generate Swagger Documentation
 * </pre>
 * 
 * @since 2019-03-15
 * @author bob
 * @version
 *          <ul>
 *          <li>2019-03-15,bob,new
 *          </ul>
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket api() {
        Parameter param = new ParameterBuilder().name("Authorization").modelRef(new ModelRef("string")).parameterType("header").required(false)
                .build();
        List<Parameter> params = new ArrayList<>();
        params.add(param);

        return new Docket(DocumentationType.SWAGGER_2).globalOperationParameters(params).select()
                .apis(RequestHandlerSelectors.basePackage("com.bob.polling.controller")).paths(PathSelectors.any()).build().apiInfo(metaInfo())
                .ignoredParameterTypes(AuthenticationPrincipal.class, CurrentUser.class);
    }

    private ApiInfo metaInfo() {
        return new ApiInfo("Polling App Api Documentation", "Spring Boot Api", "1.0", null,
                new Contact("polling-app (gitlab)", "https://gitlab.com/bob96589/polling-app", "bob96589@hotmail.com"), "Apache License Version 2.0",
                "https://www.apache.org/licesen.html", new ArrayList<>());
    }
}
