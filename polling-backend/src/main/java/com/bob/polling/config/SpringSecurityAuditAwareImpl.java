package com.bob.polling.config;

import java.util.Optional;

import org.springframework.data.domain.AuditorAware;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.bob.polling.security.UserPrincipal;

/**
 * <pre>
 * Define the value of properties annotated with @CreatedBy or @LastModifiedBy have to be.
 * </pre>
 * 
 * @since 2019-03-15
 * @author bob
 * @version
 *          <ul>
 *          <li>2019-03-15,bob,new
 *          </ul>
 */
class SpringSecurityAuditAwareImpl implements AuditorAware<String> {

    @Override
    public Optional<String> getCurrentAuditor() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication == null || !authentication.isAuthenticated() || authentication instanceof AnonymousAuthenticationToken) {
            return Optional.empty();
        }

        UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();

        return Optional.ofNullable(userPrincipal.getId());
    }
}
