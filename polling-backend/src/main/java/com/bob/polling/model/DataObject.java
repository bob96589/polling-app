package com.bob.polling.model;

public interface DataObject {
    String getId();

    void setId(String id);
}
