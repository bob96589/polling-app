# Visit website on Google Cloud Platform
[Polling App](http://104.199.155.251:8080/#/login)

[Swagger API Documentation](http://34.80.79.98:9080/api/swagger-ui.html)


# Used Libraries and Environments
### **Frontend**  
Vue cli 3.0  
Vue-router  
Vuex  
Vuelidate  
Vue-loading-overlay  
Vue-notification  
Axios  
Bootstrap 4  

### **Backend**  
Spring Boot  
Spring Security  
Json Web Token  
Restful Api  
Swagger

### **Database**  
H2  
MySQL on GCP Cloud SQL

### **Testing**  
E2E Test (Nightwatch)  
Unit Test (Jest)   
Spring Boot Integration Test

### **CI/CD**  
Jenkins

# Steps to start Polling App locally

## 1. Clone from Gitlab
```
> git clone https://gitlab.com/bob96589/polling-app.git
```
## 2. Two ways to start Polling App

```
Backend
Directory: polling-app/polling-backend
> mvn clean package
> java -jar ./target/polling-backend-0.0.1-SNAPSHOT.jar --spring.profiles.active=default

Frontend
Directory: polling-app/polling-frontend
> npm install
> npm run serve
> npm run test:e2e
> npm run test:unit
```
```
Backend
Directory: polling-app/polling-backend
> mvn clean package

Frontend
Directory: polling-app/polling-frontend
> npm install

Start service using docker-compose
Directory: polling-app
(set environment variable DEPLOY_ENV = 'default', linux for instance)
> export DEPLOY_ENV='default' 
> docker-compose up -d
```

## 3. Open browser
Swagger API Documentation
```
http://localhost:9080/api/swagger-ui.html
```


Applicaiton demo site
```
http://localhost:9090/#/login
```

# Screen Shots
<img src="./README/99_swagger.png" width="80%">
<img src="./README/01_login.png" width="80%">
<img src="./README/02_signup.png" width="80%">
<img src="./README/03_pollingList.png" width="80%">
<img src="./README/04_createPoll.png" width="80%">
<img src="./README/05_validation.png" width="80%">
<img src="./README/06_myPoll.png" width="80%">
<img src="./README/07_myVote.png" width="80%">
<img src="./README/08_adminView.png" width="80%">
<img src="./README/09_voter.png" width="80%">
<img src="./README/10_loading.gif" width="80%">
<img src="./README/11_e2eTest.gif" width="80%">
